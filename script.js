let weekArray = [];
let today = new Date()
  .toLocaleString("en-GB", { timeZone: "Europe/London" })
  .slice(0, 10)
  .split("/")
  .reverse()
  .join("-");

//today = "2023-05-29";
let weekLabel = document.querySelector(".week__label");
let weekWrapper = document.querySelector(".week__wrapper");
let openingBar = document.querySelector('.opening-bar');
let closingBar = document.querySelector('.closing-bar');
let progressBar = document.querySelector('.progress-bar');
let hoursRow = document.querySelector(".hours__row");
const slider = document.querySelector(".parent");



/////////////////////
// FETCH DATA
/////////////////
fetch("https://tesia.fly.dev/api/week")
//fetch("./event.json")
  .then((response) => response.json())
  .then((dates) => {
    dates.forEach((data) => weekArray.push(data));
  })
  .finally(() => {
    let currentDay = weekArray.find((data) => data.day === today);

    weekLabel.innerHTML = `
          ${weekArray[0].day
            .split("-")
            .reverse()
            .join("/")} - ${weekArray[6].day.split("-").reverse().join("/")}`;

    createDays(weekArray, currentDay);
    createOpeningTime(currentDay);
    createExclusions(currentDay);
    createTimeTable(currentDay);
    createRowHours()
    slider.scroll({
      top: 0,
      left: 180,
      behavior: "smooth"
    });
  });

///////////////
// CREATE DAYS
///////////
function createDays(weekArray, currentDay) {
  weekArray.forEach((day) => {
    let dayElement = createDay(day);
    dayElement.classList.add("day__container");
    if (day === currentDay) {
      dayElement.classList.add("selected");
    }
    weekWrapper.appendChild(dayElement);
  });

  function createDay(day) {
    //console.log(day);

    let dayElement = document.createElement("div");
    dayElement.innerHTML = `
                <div class="day__info ${
                  day.openTime.length === 0 || day.holiday ? "close" : ""
                }" >
                  <div class="day__info--dayName">
                    ${new Date(day.day).toString().split(" ")[0]}
                  </div>
                  <div class="day__info--fullDate">${day.day
                    .split("-")
                    .reverse()
                    .join("/")}</div>
                </div>
                <div class="day__bookingBar" style="background-color : ${
                  day.exclusions.length !== 0 && !day.holiday? "#ef4444" : "transparent"
                }">
                </div>
              `;
    dayElement.addEventListener("click", (e) => {
      progressBar.innerHTML = "";
      //openingBar.innerHTML = "";
      closingBar.innerHTML = "";
      createOpeningTime(day);
      createExclusions(day);
      createTimeTable(day);
      slider.scroll({
        top: 0,
        left: 180,
        behavior: "smooth"
      });
    });
    dayElement.style.cursor = "pointer";
    return dayElement;
  }
  

  let siblingsArray = Array.from(document.querySelectorAll(".day__container"));
  siblingsArray.forEach((day) =>
    day.addEventListener("click", () => {
      day.classList.toggle("selected");

      let siblings = getSiblings(day);

      siblings.map((e) => {
        e.classList.remove("selected");
      });
    })
  );
}

///////////////////////
/////// HOURS ROW
/////////////
function createRowHours(){
  for (let i = 0; i < 24; i++) {
    let hour = document.createElement("div");
    hour.innerHTML = `
    
      <div
        style="
              border-left: ${i !== 0 ? "1px solid #000" : "none"};
              border-right: ${i === 24 ? "1px solid #000" : "none"};
              padding-left:2px;
              height:1.5rem; 
              margin-top:1rem;
              "
      >${i}</div>`;
  
    // <div
    //   style="
    //         height:20px;
    //         background:#10b981;
    //         margin-top: 3px
    //         "
    // ><div>`;
    hoursRow.appendChild(hour);
  }
}

function diff_minutes(dt2, dt1) {
  var diff = (dt2.getTime() - dt1.getTime()) / 1000;
  diff /= 60;
  return Math.abs(Math.round(diff));
}

////////////////////
// OPENINGS
/////////////

function createOpeningTime(day) {
  if (day.openTime && !day.holiday) {
    for (let i = 0; i < day.openTime.length; i++) {
      let from = new Date(`${day.day} ${day.openTime[i].from}`);
      let to = new Date(`${day.day} ${day.openTime[i].to}`);
      let start = getStart(day.openTime[i].from);
      let typeOfBar = "opening";

      // createOpeningBar(
      //   (diff_minutes(from, to) / 1440) * 100,
      //   start,
      //   typeOfBar
      // );

      createProgressBar(
        (diff_minutes(from, to) / 1440) * 100,
        start,
        typeOfBar
      );
    }
  } else {
    progressBar.innerHTML = "";
  }
}

////////////////////
// EXCLUSION
/////////////
function createExclusions(day) {
  if (day.exclusions && !day.holiday) {
    for (let i = 0; i < day.exclusions.length; i++) {
      let from = new Date(`${day.day} ${day.exclusions[i].from}`);
      let to = new Date(`${day.day} ${day.exclusions[i].to}`);
      let start = getStart(day.exclusions[i].from);
      let typeOfBar = "exclusions";
      createClosingBar(
        (diff_minutes(from, to) / 1440) * 100,
        start,
        typeOfBar
      );
      createProgressBar(
        (diff_minutes(from, to) / 1440) * 100,
        start,
        typeOfBar
      );
    }
  }
}
function getStart(from) {
  let bo = Number(from.substr(0, 2) * 60) + Number(from.substr(3, 5));
  let ciao = (bo / 1440) * 100;
  return ciao;
}
////////////////////
// OPENING BAR
/////////////
// function createOpeningBar(length, start, typeOfBar) {
//   let openingBarElement = document.createElement("div");
//   openingBarElement.innerHTML = `
//     <div
//       style="width:${length}%;
//              height:1rem; 
//              background:${typeOfBar === "opening" ? "#10b981" : "#ef4444"}; 
//              margin-left:${start}%;
//               position:absolute;
//              ">
//     </div>`;
//   openingBar.appendChild(openingBarElement);
// }
////////////////////
// CLOSING BAR
/////////////
function createClosingBar(length, start, typeOfBar) {
  let closingBarElement = document.createElement("div");
  closingBarElement.innerHTML = `
    <div
      style="width:${length}%;
             height:1rem; 
             background:${typeOfBar === "opening" ? "#10b981" : "#ef4444"}; 
             margin-left:${start}%;
              position:absolute;
             ">
    </div>`;
  closingBar.appendChild(closingBarElement);
}
////////////////////
// PROGRESS BARS
/////////////
function createProgressBar(length, start, typeOfBar) {
  let progressBarElement = document.createElement("div");
  progressBarElement.innerHTML = `
    <div
      style="width:${length}%;
             height:1rem; 
             background:${typeOfBar === "opening" ? "#10b981" : "#cacaca"}; 
             margin-left:${start}%;
              position:absolute;
             ">
    </div>`;
  progressBar.appendChild(progressBarElement);
}

///////////////////////
// TIMETABLE
/////////////
function createTimeTable(day) {
  console.log("son qua");

  let timetable = document.querySelector(".timeTable");
  timetable.innerHTML = "";
  // holidays
  if (day.holiday) {
    let close = document.createElement("div");
    close.classList.add("openingTime");
    close.innerHTML = `Chiuso - ${day.holiday}`;
    timetable.appendChild(close);
    return
  } 

  // dataSet1
  if (day.openTime.length !== 0) {
    let aperture = document.createElement("div");
    aperture.classList.add("openingTime");
    aperture.innerHTML = "Apertura ";
    day.openTime.forEach((day) => {
      let dataSet1 = document.createElement("span");
      dataSet1.style.marginLeft = "1rem";
      dataSet1.innerHTML = `${day.from}-${day.to}`;
      aperture.appendChild(dataSet1);
    });

    timetable.appendChild(aperture);
  } else {
    let close = document.createElement("div");
    close.classList.add("openingTime");
    close.innerHTML = "Chiuso ";
    timetable.appendChild(close);
  }

  // dataSet2
  if (day.exclusions.length !== 0) {
    day.exclusions.forEach((exclusion) => {
      let dataSet2 = document.createElement("div");
      dataSet2.innerHTML = `
        <div>esclusione dalle ${exclusion.from} alle ${exclusion.to}</div>
        `;
      timetable.appendChild(dataSet2);
    });
  }
}

//////////////////
////// BUTTONS
//////////////

// button left
let container = document.querySelector(".container");
let btnLeft = document.createElement("button");
btnLeft.classList.add("btn");
btnLeft.classList.add("btn-left");
btnLeft.innerHTML = `
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.2 288 416 288c17.7 0 32-14.3 32-32s-14.3-32-32-32l-306.7 0L214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z"/></svg>
  `;
/*btnLeft.addEventListener("click", () => {
  if (mondayIndex > 0) mondayIndex -= 7;
  createWeekArray();
});*/
container.appendChild(btnLeft);

// button right
let btnRight = document.createElement("button");
btnRight.classList.add("btn");
btnRight.classList.add("btn-right");
btnRight.innerHTML = `
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M438.6 278.6c12.5-12.5 12.5-32.8 0-45.3l-160-160c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L338.8 224 32 224c-17.7 0-32 14.3-32 32s14.3 32 32 32l306.7 0L233.4 393.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0l160-160z"/></svg>
  `;
/*btnRight.addEventListener("click", () => {
  mondayIndex += 7;
  createWeekArray();
});*/
container.appendChild(btnRight);

///////////////////
/// SIBLINGS
////////////
function getSiblings(e) {
  // for collecting siblings
  let siblings = [];
  // if no parent, return no sibling
  if (!e.parentNode) {
    return siblings;
  }
  // first child of the parent node
  let sibling = e.parentNode.children[0];
  // collecting siblings
  while (sibling) {
    if (sibling.nodeType === 1 && sibling !== e) {
      siblings.push(sibling);
    }
    sibling = sibling.nextSibling;
  }
  //console.log(siblings);
  return siblings;
}

////////////////////////////////
// HORIZONTAL SCROLL
/////////////////////
let mouseDown = false;
let startX, scrollLeft;

let startDragging = function (e) {
  mouseDown = true;
  startX = e.pageX - slider.offsetLeft;
  scrollLeft = slider.scrollLeft;
  /////////////
  console.log(startX, scrollLeft, e.pageX);
};
let stopDragging = function (event) {
  mouseDown = false;
};

slider.addEventListener("mousemove", (e) => {
  e.preventDefault();
  if (!mouseDown) {
    return;
  }
  const x = e.pageX - slider.offsetLeft;
  const scroll = x - startX;
  slider.scrollLeft = scrollLeft - scroll;
});

// Add the event listeners
slider.addEventListener("mousedown", startDragging, false);
slider.addEventListener("mouseup", stopDragging, false);
slider.addEventListener("mouseleave", stopDragging, false);

// slider.scroll({
//   top: 0,
//   left: 180,
//   behavior: "smooth"
// });
